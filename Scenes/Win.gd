extends Area2D

export(String) var scene_to_load

func _on_Win_body_entered(body):
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	
