extends KinematicBody2D

export (int) var speed = 300
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var on_ground = true
var jump_counter = 0
var diam = true
var walking = false

func get_input():
	var animation = "Idle"
	velocity.x = 0
	if Input.is_action_just_pressed("ui_up"):
		on_ground = false
		walking = false
		if jump_counter < 1:
			jump_counter += 1
			velocity.y = jump_speed
			on_ground = false
			
	if Input.is_action_pressed("ui_right"):
		velocity.x += speed
		diam = false
		walking = true
		animation = "Walk"
		$AnimatedSprite.flip_h = false
		
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		diam = false
		walking = true
		animation = "Walk"
		$AnimatedSprite.flip_h = true
		
	if is_on_floor():
		on_ground = true
		diam = true
		jump_counter = 0
		walking = false
	
	if !is_on_floor():
		animation = "Jump"
		walking = false
		
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

func attacked():
	velocity.y = 1.3 * jump_speed
	
func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
