extends Area2D

export (String) var sceneName = "Level 1"

func _on_Mushroom_Evil_body_entered(body):
	var current_scene = get_tree().get_current_scene().get_name()
	if body.get_name() == "Player":
		if current_scene == sceneName:
			global.lives -= 3
			queue_free()
		if (global.lives <= 0):
			global.lives = 3
			get_tree().change_scene(str("res://Scenes/Game Over.tscn"))
		else:
			body.attacked()
