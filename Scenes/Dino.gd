extends KinematicBody2D

export (int) var speed = 300
export (int) var GRAVITY = 1200
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var direction = 1
var move = true

func _ready():
	pass
	
func _physics_process(delta):
	if move:
		velocity.x = speed * direction
		
		if direction == 1:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
			
		$AnimatedSprite.play("Walk")
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, FLOOR)
		
		if is_on_wall():
			direction *= -1
			$RayCast2D.position.x *= -1
			
		if $RayCast2D.is_colliding() == false:
			direction *= -1
			$RayCast2D.position.x *= -1
